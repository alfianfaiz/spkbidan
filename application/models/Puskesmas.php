<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puskesmas extends CI_Model {

	public function getKelasKriteriaPuskemas($puskesmas_id,$kriteria)
	{
		$this->db->where('puskesmas.id', $puskesmas_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->join('puskesmas_kriteria', 'puskesmas.id = puskesmas_kriteria.puskesmas_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = puskesmas_kriteria.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$data = $this->db->get('puskesmas')->result();
		$result = 0;
		foreach ($data as $key) {
			$result = $key->nama_kelas;
		}
		return $result;
	}

	public function getDataPuskesmas($puskesmas_id)
	{
		$this->db->where('puskesmas.id', $puskesmas_id);
		$data = $this->db->get('puskesmas')->result();
		return $data;
	}

	public function getIDKelasKriteriaPuskesmas($puskesmas_id,$kriteria)
	{
		$this->db->where('puskesmas.id', $puskesmas_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->join('puskesmas_kriteria', 'puskesmas.id = puskesmas_kriteria.puskesmas_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = puskesmas_kriteria.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kelas_kriteria.id as ID');
		$data = $this->db->get('puskesmas')->result();
		$result = 0;
		foreach ($data as $key) {
			$result = $key->ID;
		}
		return $result;
	}

	public function deleteKriteriaNilaiByKriteria($puskesmas_id,$kriteria)
	{
		$this->db->join('puskesmas_kriteria', 'puskesmas.id = puskesmas_kriteria.puskesmas_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = puskesmas_kriteria.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->where('puskesmas.id', $puskesmas_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->select('puskesmas_kriteria.id');
		$data = $this->db->get('puskesmas')->result();
		return $data;
		// $this->db->delete('bidan_kriteria_nilai');
	}

}

/* End of file Puskesmas.php */
/* Location: ./application/models/Puskesmas.php */