<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('IS_LOGGED_IN'))
		{
			$data = array(
				"page" => 'home'
				);
			$this->load->view('layout/mainlayout', $data);
		}
			else 
		{
			$data = array(
			"page" => 'home'
			);
			$this->load->view('layout/mainlayoutv2', $data);
		}
	}

	public function panduan()
	{
		$data = array(
			"page" => 'panduan'
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function kontak()
	{
		$data = array(
			"page" => 'kontak'
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function actionKontak()
	{
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$pesan = $this->input->post('pesan');

		$data = array(
			'nama' => $nama,
			'email' => $email,
			'pesan' => $pesan,
			);
		$this->db->insert('kontak', $data);
		redirect('home/kontak','refresh');
	}

	public function pendaftaran()
	{
		$this->load->model('Umum');
		$model_umum = new Umum();

		$data = array(
			"page" => 'pendaftaran',
			'model_umum' => $model_umum,
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function kelolav1()
	{
		$this->load->model('Bidan');
		$model_bidan = new Bidan();
		$bidan_id = $this->session->userdata('id_user');
		$data = array(
			"page" => 'datapendaftarpribadi',
			"model_bidan" => $model_bidan,
			"bidan_id" => $bidan_id,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function hasilpenempatanv1()
	{
		$this->load->model('Bidan');
		$model_bidan = new Bidan();
		$data = array(
			"page" => 'hasilpenempatanbidan',
			"model_bidan" => $model_bidan,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function selamat($string)
	{
		$data = array(
			"page" => 'selamat',
			"string" => $string,
			);
		$this->load->view('layout/mainlayout', $data);
	}
	
	public function daftarBidanMendaftar()
	{
		if($this->input->is_ajax_request())
		{
			$puskesmas_id = $this->input->post('puskesmas_id');
			$this->load->model('Bidan');
			$daftar_bidan = $this->Bidan->getAllBidanByPuskesmas($puskesmas_id);
			$data = array(
				'daftar_bidan' => $daftar_bidan,
				);
			$this->load->view('page/partial_hasilpenempatanpetugas', $data);
			// echo "yes";
		}
	}

	public function konversiSkor($gap)
	{
		
		$skor = array(
			'0' => 7,
			'1' => 6,
			'-1' => 6.5,
			'2' => 5.5,
			'-2' => 5,
			'3' => 4.5,
			'-3' => 4,
			'4' => 3.5,
			'-4' => 3,
			'5' => 2.5,
			'-5' => 2,
			'6' => 1.5,
			'-6' => 1
			);
		return $skor[$gap];
	}

	public function register()
	{
		$this->load->helper('inflector');
		$nama_pendaftar = $this->input->post('nama_pendaftar');
		$nik_pendaftar = $this->input->post('nik_pendaftar');
		$tlahir_pendaftar = $this->input->post('tlahir_pendaftar');
		$agama_pendaftar = $this->input->post('agama_pendaftar');
		$alamat_pendaftar = $this->input->post('alamat_pendaftar');
		$puskesmas_id = $this->input->post('puskesmas_id');
		$data_kriteria = $this->input->post('datakriteria');
		$kode_pendaftaran = $this->generateCode();
		$password = $this->input->post('password');

		$fileName = date('Ymdhis').'.jpg';
		$config['upload_path'] 			= './uploads/';
		$config['allowed_types']        = 'png|jpg|jpeg|bmp';
        $config['max_size']             = 2000;
        $config['file_name']            = $fileName;
        $config['overwrite']			= TRUE;

        // initializing library upload after config
        $this->load->library('upload', $config);

        // uploading file
        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            $message = "";
            foreach ($error as $key) {
            	echo $message .= "<li>".$key."</li>";
            }
            // $this->session->set_flashdata('error',$message);
            // redirect('home/pendaftaran');
        }

		$data_pendaftar = array(
			'nama'=>$nama_pendaftar,
			'nik' =>$nik_pendaftar,
			'tanggal_lahir' =>$tlahir_pendaftar,
			'agama' =>$agama_pendaftar,
			'alamat' =>$alamat_pendaftar,
			'puskesmas_id' =>$puskesmas_id,
			'kode_pendaftaran' => $kode_pendaftaran,
			'image' => $fileName,
			'password' => $password,
			);
		$this->db->insert('bidan', $data_pendaftar);
		$bidan_id = $this->db->insert_id();

		// $skor_pskesmas = $this->getPuskesmasBobot($puskesmas_id,'Jarak 1');

		$this->db->where('puskesmas_kriteria.puskesmas_id', $puskesmas_id);
		$this->db->join('kelas_kriteria', 'puskesmas_kriteria.kelas_kriteria_id = kelas_kriteria.id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kriteria.kriteria,kelas_kriteria.skor,kelas_kriteria.id');
		$query = $this->db->get('puskesmas_kriteria')->result();
		print_r($data_kriteria);
		foreach ($query as $key) {
			if($key->kriteria == "Uji Kompetensi 1")
				break;
			// echo $key->skor." ";
			// echo $this->getscore($data_kriteria[underscore(strtolower($key->kriteria))])." ";
			$gap = $this->getscore($data_kriteria[underscore(strtolower($key->kriteria))]) - $key->skor;
			// echo "<br>";

			$data_kriteria_bidan = array(
			'bidan_id' => $bidan_id,
			'kelas_kriteria_id' => $key->id,
			'skor' => $this->konversiSkor($gap),
			'gap' => $gap,
			);
			
			$this->db->insert('bidan_kriteria_nilai', $data_kriteria_bidan);
		}

		$kode_pendaftaran;
		$nama_pendaftar;
		$nik_pendaftar;
		$string = "Selamat anda telah terdaftar dengan NAMA : $nama_pendaftar dan KODE PENDAFTARAN : $kode_pendaftaran";
		$array = array(
			'nama' => $nama_pendaftar,
			'kode_pendaftaran' => $kode_pendaftaran,
			'nik_pendaftar'=>$nik_pendaftar
		);
		
		$this->session->set_userdata( $array );
		$this->selamat($string);
	}

	

	public function generateCode(){
		// $this->load->library('database');
		$this->load->library('RandomCode');
		$randomCode = new RandomCode;
		$RC = $randomCode->RandomPass(5,3);
		$this->db->where('kode_pendaftaran',$RC);
		if($this->db->count_all_results('bidan') == 0)
			return $RC;
		else $this->generateCode();
	}

	private function getscore($id)
	{
		$this->db->where('id',$id);
		$this->db->select('skor');
		$query = $this->db->get('kelas_kriteria')->result();
		return $query[0]->skor;
	}

	private function getPuskesmasBobot($puskesmas_id,$kriteria)
	{
		$this->db->where('puskesmas_kriteria.puskesmas_id', $puskesmas_id);
		$this->db->where('kriteria.kriteria',$kriteria);
		$this->db->join('kelas_kriteria', 'puskesmas_kriteria.kelas_kriteria_id = kelas_kriteria.id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kelas_kriteria.skor as skor');
		$query = $this->db->get('puskesmas_kriteria')->result();
		// print_r($query);
		return $query[0]->skor;
	}
	private function ceklogin()
	{
		if ($this->session->userdata('level') == 2) {
			redirect('app2','refresh');
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */