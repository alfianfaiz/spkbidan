<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('IS_LOGGED_IN') || $this->session->userdata('id_user') != "ADMIN" )
		{
			$data = array(
				"page" => 'home'
				);
			$this->load->view('layout/mainlayout', $data);
		}
		
	}

	public function index()
	{
		$data = array(
		"page" => 'home'
		);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function inputdatakompetensi()
	{
		$this->load->model('Bidan');
		$this->load->model('Umum');
		$model_umum = new Umum();
		$model_bidan = new Bidan();
		$data = array(
			"page" => 'inputdatakompetensi',
			'model_umum' => $model_umum,
			'model_bidan' => $model_bidan,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function hasilpenempatanv2()
	{
		$keyword = $this->input->post('keyword');
		$this->load->model('Bidan');
		$model_bidan = new Bidan();
		$daftar_bidan = $model_bidan->getAllBidanByPuskesmas(null,$keyword);
		$data = array(
			"page" => 'hasilpenempatanpetugas',
			"daftar_bidan" => $daftar_bidan,
			"keyword" => $keyword,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function perhitunganNilaiAkhir($bidan_id)
	{
		$this->load->model('Bidan');
		$data_bidan = $this->Bidan->getDataBidan($bidan_id);
		// print_r($data_bidan);
		// PERHITUNGAN JARAK
		$j1 = $this->Bidan->getSkorKriteria($bidan_id,'Jarak 1');
		$j2 = $this->Bidan->getSkorKriteria($bidan_id,'Jarak 1');
		
		

		// PERHITUNGAN EVALUASI
		$ev1 = $this->Bidan->getSkorKriteria($bidan_id,'Evaluasi Diri 1');
		$ev2 = $this->Bidan->getSkorKriteria($bidan_id,'Evaluasi Diri 2');
		$ev3 = $this->Bidan->getSkorKriteria($bidan_id,'Evaluasi Diri 3');
		$ev4 = $this->Bidan->getSkorKriteria($bidan_id,'Evaluasi Diri 4');

		
		
		// PERHITUNGAN PENGALAMAN KERJA
		$P1 = $this->Bidan->getSkorKriteria($bidan_id,'Pengalaman Kerja 1');
		$P2 = $this->Bidan->getSkorKriteria($bidan_id,'Pengalaman Kerja 2');
		
		
		// PERHITUNGAN UJI KOMPETENSI
		$UK1 = $this->Bidan->getSkorKriteria($bidan_id,'Uji Kompetensi 1');
		$UK2 = $this->Bidan->getSkorKriteria($bidan_id,'Uji Kompetensi 2');
		$UK3 = $this->Bidan->getSkorKriteria($bidan_id,'Uji Kompetensi 3');
		
		
		$NCF = $this->average(array($j1,$ev2,$ev3,$ev4,$P1,$UK1,$UK2));
		$NSF = $this->average(array($j2,$ev1,$P2,$UK3));
		
		

		$NA = 0.6 * $NCF + 0.4 * $NSF;
		$this->Bidan->updateNilaiAkhir($NA,$bidan_id);
		return TRUE;
	}

	public function actionTerima($bidan_id)
	{
		$this->db->where('id', $bidan_id);
		$this->db->set('status_diterima',1);
		$this->db->update('bidan');
		redirect($this->agent->referrer());
	}

	public function actionBatalkan($bidan_id)
	{
		$this->db->where('id', $bidan_id);
		$this->db->set('status_diterima',0);
		$this->db->update('bidan');
		redirect($this->agent->referrer());
	}

	public function editDataBidan($bidan_id)
	{
		$this->load->model('Bidan');
		$this->load->model('Umum');
		$model_umum = new Umum();
		$model_bidan = new Bidan();
		$data = array(
			"page" => 'edit_data_bidan',
			'model_umum' => $model_umum,
			'model_bidan' => $model_bidan,
			'bidan_id' => $bidan_id,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function actionEditDataBidan()
	{
		$this->load->helper('inflector');
		$bidan_id = $this->input->post('bidan_id');
		$nama_pendaftar = $this->input->post('nama_pendaftar');
		$nik_pendaftar = $this->input->post('nik_pendaftar');
		$agama_pendaftar = $this->input->post('agama_pendaftar');
		$alamat_pendaftar = $this->input->post('alamat_pendaftar');
		$puskesmas_id = $this->input->post('puskesmas_id');
		$kode_pendaftaran = $this->input->post('kode_pendaftaran');
		$data_kriteria = $this->input->post('datakriteria');

		$data_pendaftar = array(
			'nama'=>$nama_pendaftar,
			'nik' =>$nik_pendaftar,
			'agama' =>$agama_pendaftar,
			'alamat' =>$alamat_pendaftar,
			'puskesmas_id' =>$puskesmas_id,
			);
		$this->db->where('id', $bidan_id);
		$this->db->update('bidan', $data_pendaftar);


		$this->db->where('puskesmas_kriteria.puskesmas_id', $puskesmas_id);
		$this->db->join('kelas_kriteria', 'puskesmas_kriteria.kelas_kriteria_id = kelas_kriteria.id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kriteria.kriteria,kelas_kriteria.skor,kelas_kriteria.id as ID');
		$query = $this->db->get('puskesmas_kriteria')->result();
		// print_r($data_kriteria);
		foreach ($query as $key2) {
			$this->deleteNilaiKriteriaBidan($kode_pendaftaran,$key2->kriteria);
			// echo $key->skor." ";
			$this->getscore($data_kriteria[underscore(strtolower($key2->kriteria))])." ";
			$key2->skor." ";
			$gap = $this->getscore($data_kriteria[underscore(strtolower($key2->kriteria))]) - $key2->skor;
			$this->konversiSkor($gap);
			// // 

			$data_kriteria_bidan = array(
			'bidan_id' => $bidan_id,
			'kelas_kriteria_id' => $data_kriteria[underscore(strtolower($key2->kriteria))],
			'skor' => $this->konversiSkor($gap),
			'gap' => $gap,
			);
			$this->db->insert('bidan_kriteria_nilai', $data_kriteria_bidan);
		}

		$this->perhitunganNilaiAkhir($bidan_id);
		// $bidan_id = $this->db->insert_id();
		// redirect($this->agent->referrer(),'refresh');
	}

	public function actionEditDataPuskesmas()
	{
		$this->load->helper('inflector');
		$puskesmas_id = $this->input->post('puskesmas_id');
		$kelurahan = $this->input->post('kelurahan');
		$data_puskesmas = $this->input->post('data_puskesmas'); 

		$data_puskemas_update = array(
			'kelurahan' => $kelurahan,
			);
		$this->db->where('id', $puskesmas_id);
		$this->db->update('puskesmas', $data_puskemas_update);


		$this->db->where('puskesmas_kriteria.puskesmas_id', $puskesmas_id);
		$this->db->join('kelas_kriteria', 'puskesmas_kriteria.kelas_kriteria_id = kelas_kriteria.id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kriteria.kriteria,kelas_kriteria.skor,kelas_kriteria.id as ID');
		$query = $this->db->get('puskesmas_kriteria')->result();
		// // print_r($data_kriteria);
		foreach ($query as $key2) {
			// echo $key->skor." ";
			// $this->getscore($data_kriteria[underscore(strtolower($key2->kriteria))])." ";
			// // // echo "<br>";
			$data_kriteria_bidan = array(
			'puskesmas_id' => $puskesmas_id,
			'kelas_kriteria_id' => $data_puskesmas[underscore(strtolower($key2->kriteria))],
			);
			$this->deleteNilaiKriteriaPuskesmas($puskesmas_id,$key2->kriteria);
			$this->db->insert('puskesmas_kriteria', $data_kriteria_bidan);
		}

		$this->perhitunganNilaiAkhir($puskesmas_id);
		// // $bidan_id = $this->db->insert_id();
		redirect($this->agent->referrer(),'refresh');
	}

	public function konversiSkor($gap)
	{
		
		$skor = array(
			'0' => 7,
			'1' => 6.5,
			'-1' => 6,
			'2' => 5.5,
			'-2' => 5,
			'3' => 4.5,
			'-3' => 4,
			'4' => 3.5,
			'-4' => 3,
			'5' => 2.5,
			'-5' => 2,
			'6' => 1.5,
			'-6' => 1
			);
		return $skor[$gap];
	}

	function average($arr)
	{
	    if (!count($arr)) return 0;

	    $sum = 0;
	    for ($i = 0; $i < count($arr); $i++)
	    {
	        $sum += $arr[$i];
	    }

	    return $sum / count($arr);
	}

	public function deleteNilaiKriteriaBidan($kode_pendaftaran,$kriteria)
	{
		$this->load->model('Bidan');
		$listId = $this->Bidan->deleteKriteriaNilaiByKriteria($kode_pendaftaran,$kriteria);
		foreach ($listId as $key) {
			$this->db->where('id', $key->id);
			$this->db->delete('bidan_kriteria_nilai');
		}
	}

	public function deleteNilaiKriteriaPuskesmas($puskesmas_id,$kriteria)
	{
		$this->load->model('Puskesmas');
		$listId = $this->Puskesmas->deleteKriteriaNilaiByKriteria($puskesmas_id,$kriteria);
		foreach ($listId as $key) {
			$this->db->where('id', $key->id);
			$this->db->delete('puskesmas_kriteria');
		}
	}

	public function actioninputdatakompetensi()
	{
		$kode_pendaftaran = $this->input->post('kode_pendaftaran');
		$this->deleteNilaiKriteriaBidan($kode_pendaftaran,'Uji Kompetensi 1');
		$this->deleteNilaiKriteriaBidan($kode_pendaftaran,'Uji Kompetensi 2');
		$this->deleteNilaiKriteriaBidan($kode_pendaftaran,'Uji Kompetensi 3');
		$bidan = $this->db->where('kode_pendaftaran', $kode_pendaftaran)->get('bidan')->result();

		foreach ($bidan as $key) {
			$kap = $this->input->post('kap')." ";
			$kr = $this->input->post('kr')." ";
			$kpi = $this->input->post('kpi')." ";
			$skor_pskesmas_kap = $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 1')." ";
			$skor_pskesmas_kr = $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 2')." ";
			$skor_pskesmas_kpi = $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 3')." ";
			// $this->getscore($kap)." ";
			// $this->getscore($kr)." ";
			// $this->getscore($kpi)." ";
			$skor_bidan_kap = $this->getscore($kap) - $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 1');
			$skor_bidan_kr = $this->getscore($kr) - $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 2');
			$skor_bidan_kpi = $this->getscore($kpi) - $this->getPuskesmasBobot($key->puskesmas_id,'Uji Kompetensi 3');

			$data_kriteria_bidan = array(
			'bidan_id' => $key->id,
			'kelas_kriteria_id' => $kap,
			'gap' => $skor_bidan_kap,
			'skor' => $this->konversiSkor($skor_bidan_kap),
			);
			$this->db->insert('bidan_kriteria_nilai', $data_kriteria_bidan);

			$data_kriteria_bidan = array(
			'bidan_id' => $key->id,
			'kelas_kriteria_id' => $kr,
			'skor' => $this->konversiSkor($skor_bidan_kr),
			'gap' => $skor_bidan_kr,
			);
			$this->db->insert('bidan_kriteria_nilai', $data_kriteria_bidan);

			$data_kriteria_bidan = array(
			'bidan_id' => $key->id,
			'kelas_kriteria_id' => $kpi,
			'skor' => $this->konversiSkor($skor_bidan_kpi),
			'gap' => $skor_bidan_kpi,
			);
			$this->db->insert('bidan_kriteria_nilai', $data_kriteria_bidan);
			$this->perhitunganNilaiAkhir($key->id);
			redirect('admin/inputdatakompetensi','refresh');
		}
		
	}



	public function puskesmas()
	{
		$data_puskesmas = $this->db->get('puskesmas')->result();
		$data = array(
			"page" => 'puskesmas',
			"data_puskesmas" => $data_puskesmas,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function detilpuskesmas($puskesmas_id)
	{
		$data_puskesmas = $this->db->where('status','1')->where('id',$puskesmas_id)->get('puskesmas')->result();
		$this->load->model('Puskesmas');
		$model_puskesmas = new Puskesmas();
		$data = array(
			"page" => 'detilpuskesmas',
			"data_puskesmas" => $data_puskesmas,
			'model_puskesmas' => $model_puskesmas,
			'puskesmas_id' => $puskesmas_id,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function disablepuskesmas($puskesmas_id)
	{
		$this->db->set('status',0);
		$this->db->where('id', $puskesmas_id);
		$this->db->update('puskesmas');
		redirect($this->agent->referrer(),'refresh');
	}

	public function enablepuskesmas($puskesmas_id)
	{
		$this->db->set('status',1);
		$this->db->where('id', $puskesmas_id);
		$this->db->update('puskesmas');
		redirect($this->agent->referrer(),'refresh');
	}

	public function editpuskesmas($puskesmas_id)
	{
		$this->load->model('Puskesmas');
		$this->load->model('Umum');
		$model_umum = new Umum();
		$model_puskesmas = new Puskesmas();
		$data = array(
			"page" => 'edit_data_puskesmas',
			'model_umum' => $model_umum,
			'model_puskesmas' => $model_puskesmas,
			'puskesmas_id' => $puskesmas_id,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function tambah_puskesmas()
	{
		
		$this->load->model('umum');
		$model_umum = new Umum();
		$data = array(
			"page" => 'tambah_puskesmas',
			'model_umum' => $model_umum,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function kelolav2()
	{
		$this->load->model('Bidan');
		$model_bidan = new Bidan();
		$data = array(
			"page" => 'daftarpendaftar',
			"model_bidan" => $model_bidan,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function detilpendaftarv2($bidan_id)
	{
		$this->load->model('Bidan');
		$model_bidan = new Bidan();
		$data = array(
			"page" => 'detilpendaftarv2',
			"model_bidan" => $model_bidan,
			"bidan_id" => $bidan_id,
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	public function hapusbidan($bidan_id)
	{
		$this->db->where('id', $bidan_id);
		$this->db->delete('bidan');
		redirect('admin/kelolav2');
	}

	public function simpanpuskesmasbaru()
	{
		$datapuskesmas = array('kelurahan' => $this->input->post('kelurahan'),'status'=>1);
		$jarak1 = $this->input->post('jarak1');
		$jarak2 = $this->input->post('jarak2');
		$evaluasi1 = $this->input->post('evaluasi1');
		$evaluasi2 = $this->input->post('evaluasi2');
		$evaluasi3 = $this->input->post('evaluasi3');
		$evaluasi4 = $this->input->post('evaluasi4');
		$pengalamankerja1 = $this->input->post('pengalamankerja1');
		$pengalamankerja2 = $this->input->post('pengalamankerja2');
		$ujikompetensi1 = $this->input->post('ujikompetensi1');
		$ujikompetensi2 = $this->input->post('ujikompetensi2');
		$ujikompetensi3 = $this->input->post('ujikompetensi3');

		$this->db->insert('puskesmas',$datapuskesmas);
		$puskesmas_id = $this->db->insert_id();
		$this->insertPuskesmasKriteria($puskesmas_id,$jarak1);
		$this->insertPuskesmasKriteria($puskesmas_id,$jarak2);
		$this->insertPuskesmasKriteria($puskesmas_id,$evaluasi1);
		$this->insertPuskesmasKriteria($puskesmas_id,$evaluasi2);
		$this->insertPuskesmasKriteria($puskesmas_id,$evaluasi3);
		$this->insertPuskesmasKriteria($puskesmas_id,$evaluasi4);
		$this->insertPuskesmasKriteria($puskesmas_id,$pengalamankerja1);
		$this->insertPuskesmasKriteria($puskesmas_id,$pengalamankerja2);
		$this->insertPuskesmasKriteria($puskesmas_id,$ujikompetensi1);
		$this->insertPuskesmasKriteria($puskesmas_id,$ujikompetensi2);
		$this->insertPuskesmasKriteria($puskesmas_id,$ujikompetensi3);
		redirect('admin/puskesmas');
	}


	private function insertPuskesmasKriteria($puskesmas_id,$kelas_kriteria_id)
	{
		$data = array(
			'puskesmas_id' => $puskesmas_id,
			'kelas_kriteria_id' => $kelas_kriteria_id,
			);
		$this->db->insert('puskesmas_kriteria', $data);
	}
	

	public function kriteria()
	{
		$data = array(
			"page" => 'kriteria',
			);
		$this->load->view('layout/mainlayoutv2', $data);
	}

	private function getscore($id)
	{
		$this->db->where('id',$id);
		$this->db->select('skor');
		$query = $this->db->get('kelas_kriteria')->result();
		$result = 0;
		foreach ($query as $key) {
			$result = $key->skor;
		}
		return $result;
	}

	private function getPuskesmasBobot($puskesmas_id,$kriteria)
	{
		$this->db->where('puskesmas_kriteria.puskesmas_id', $puskesmas_id);
		$this->db->where('kriteria.kriteria',$kriteria);
		$this->db->join('kelas_kriteria', 'puskesmas_kriteria.kelas_kriteria_id = kelas_kriteria.id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kelas_kriteria.skor as skor');
		$query = $this->db->get('puskesmas_kriteria')->result();
		// print_r($query);
		return $query[0]->skor;
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */