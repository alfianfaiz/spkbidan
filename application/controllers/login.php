<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('IS_LOGGED_IN')) {
        			redirect('home','refresh');
        		}
		$data = array(
			"page" => 'login'
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function loginme()
	{
		if($this->input->post('nik') == "admin" && $this->input->post('password') == "adminspk")
		{
			
			$dataDB=array(
				'IS_LOGGED_IN' =>TRUE , 
				'id_user'=> 0, 
				'nama'=> "ADMIN", 
			);
			$this->session->set_userdata($dataDB);
	
			if ($this->session->userdata('IS_LOGGED_IN')) {
    			redirect('admin','refresh');
    		}
		}
		if($_POST){			
			$this->form_validation->set_rules('nik', 'Nik', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == TRUE) {
        		$this->db->where('nik',$this->input->post('nik'));
        		$this->db->where('password',$this->input->post('password'));
        		$query = $this->db->get('bidan');
        		foreach ($query->result() as $q) {
        			$dataDB=array(
						'IS_LOGGED_IN' =>TRUE , 
						'id_user'=> $q->id, 
						'nama'=> $q->nama, 
					);
					$this->session->set_userdata($dataDB);
        		}
        		if ($this->session->userdata('IS_LOGGED_IN')) {
        			redirect('home','refresh');
        		}
        		$this->session->set_flashdata('error', 'Nama atau Kode Pendaftaran yang anda masukkan salah');
				$this->index();

        	} else {
        		$this->session->set_flashdata('error', 'Nama atau Kode Pendaftaran yang anda masukkan salah');
        		$this->index();
        	}
		} 
		else
			redirect('login/');
	}

	public function out()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */