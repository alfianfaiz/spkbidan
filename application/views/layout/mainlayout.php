<?php 
	$asset = base_url().'assets/';
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/costom.css">
	<title>Sistem Pendukung Keputusan</title>
</head>

<body>
	<div class="container">

	<!-- header -->
	<div  class="header">
	<div class="row">
	  <div class="col-md-1">
	  	<img class="imageku" src="<?php echo $asset ?>image/logo.png" alt="..." class="img-circle" width="75" height="75">
	  	</div>
	  <div class="col-md-4">
	  	<div class="teks_header">
	  		SISTEM<br>PENDUKUNG<br>KEPUTUSAN
	  	</div>
	  	</div>
	</div>
	</div>
		
		<div class="navbar navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li><a class="glyphicon glyphicon-home" href="<?php echo base_url() ?>home/" > Beranda</a></li>
					<li><a class="glyphicon glyphicon-th-large" href="<?php echo base_url() ?>home/panduan"> Panduan</a></li>
					<li><a class="glyphicon glyphicon-envelope" href="<?php echo base_url() ?>home/kontak"> Kontak Kami</a></li>
					<?php if (!$this->session->userdata('IS_LOGGED_IN')): ?>
						<li><a class="glyphicon glyphicon-edit" href="<?php echo base_url() ?>home/pendaftaran"> Pendaftaran</a></li>
						<li><a class="glyphicon glyphicon-log-in" href="<?php echo base_url() ?>login/"> Login</a></li>
					<?php endif ?>
					
				</ul>
			</div>
		</div>
		<div class="div1">
		<?php $this->load->view('page/'.$page) ?>
		</div>
		<div class="footerku">
			<center ><span class="teks_footer">SPK Kelompok 2</span></center>
		</div>

	</div>
</body>
</html>