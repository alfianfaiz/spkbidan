<?php 
	$asset = base_url().'assets/';
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/costom.css">
	<script type="text/javascript" src="<?php echo $asset ?>js/jquery-2.1.4.js"></script>
	<title>Sistem Pendukung Keputusan</title>
</head>
<body>

<div class="container">
<!-- header -->
	<div  class="header">
	<div class="row">
	  <div class="col-md-1">
	  	<img class="imageku" src="<?php echo $asset ?>image/logo.png" alt="..." class="img-circle" width="75" height="75">
	  	</div>
	  <div class="col-md-4">
	  	<div class="teks_header">
	  		SISTEM<br>PENDUKUNG<br>KEPUTUSAN
	  	</div>
	  	</div>
	</div>
	</div>
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a class="glyphicon glyphicon-home" href="<?php echo base_url() ?>home/"> Beranda</a></li>
				<li><a class="glyphicon glyphicon-th-large" href="<?php echo base_url() ?>home/panduan"> Panduan</a></li>
				<li><a class="glyphicon glyphicon-envelope" href="<?php echo base_url() ?>home/kontak"> Kontak Kami</a></li>
				<?php if (!$this->session->userdata('IS_LOGGED_IN')): ?>
					<li><a class="glyphicon glyphicon-edit" href="<?php echo base_url() ?>home/pendaftaran"> Pendaftaran</a></li>
					<li><a href="<?php echo base_url() ?>login/">Login</a></li>
				<?php endif ?>
				
			</ul>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3" id="sidebar">
		<div class="div1b">
			<ul class="nav nav-pills nav-stacked">
				<?php if ($this->session->userdata('IS_LOGGED_IN') && $this->session->userdata('id_user') == "ADMIN"): ?>
					<li><a class="glyphicon glyphicon-pencil" href="<?php echo base_url() ?>admin/inputdatakompetensi"> Input Kompetensi</a></li>
					<li><a class="glyphicon glyphicon-unchecked" href="<?php echo base_url() ?>admin/hasilpenempatanv2"> Hasil Penempatan</a></li>
					<li><a class="glyphicon glyphicon-user" href="<?php echo base_url() ?>admin/kelolav2"> Data Pendaftar</a></li>
					<li><a class="glyphicon glyphicon-list-alt" href="<?php echo base_url() ?>admin/kriteria"> Data Kriteria</a></li>
					<li><a class="glyphicon glyphicon-duplicate" href="<?php echo base_url() ?>admin/puskesmas"> Data Puskesmas</a></li>
					<li><a class="glyphicon glyphicon-log-out" href="<?php echo base_url() ?>login/out"> Logout</a></li>
				<?php else: ?>
					<li><a class="glyphicon glyphicon-user" href="<?php echo base_url() ?>home/kelolav1"> Data Pribadi</a></li>
					<li><a class="glyphicon glyphicon-unchecked" href="<?php echo base_url() ?>home/hasilpenempatanv1"> Hasil Penempatan</a></li>
					<li><a class="glyphicon glyphicon-log-out" href="<?php echo base_url() ?>login/out"> Logout</a></li>
				<?php endif ?>
			</ul>	
		</div>
		</div>
		<div class="col-md-9">
		<div class="div1b">
			<?php $this->load->view('page/'.$page) ?>
			</div>
		</div>
		</div>
		
	

	<div class="footerku">
		<center ><span class="teks_footer">SPK Kelompok 2</span></center>
	</div>
</div>
</body>
</html>