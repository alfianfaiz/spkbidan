<div class="container-fluid">
	<table class="table">
		<tr>
			<td>No.</td>
			<td>Nama Bidan</td>
			<td>Kode</td>
			<td>Nilai Akhir</td>
			<td>Aksi</td>
		</tr>
		<?php $no = 1; ?>
		<?php foreach ($model_bidan->getAllBidan() as $key): ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $key->nama ?></td>
				<td><?php echo $key->kode_pendaftaran ?></td>
				<td><?php echo $key->nilai_akhir ?></td>
				<td><a href="<?php echo base_url() ?>admin/detilpendaftarv2/<?php echo $key->id ?>"><button>Detil</button></a> | <a href="<?php echo base_url() ?>admin/hapusbidan/<?php echo $key->id ?>"><button>Hapus</button></a></td>
			</tr>
		<?php endforeach ?>
	</table>
</div>