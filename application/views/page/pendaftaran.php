
<div class="div4">
<center><h2 class="marginku" id="teks_pendaftaran">Pendaftaran</h2></center>
<form class="form-horizontal" method="post" action="<?php echo base_url() ?>home/register/" style="width:700px"  enctype="multipart/form-data">
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label" >Kelurahan</label>
	    <div class="col-sm-8">
	      <select name="puskesmas_id" class="form-control" required>
	      	<?php $model_umum->getListPuskesmas(); ?>
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Nama Pendaftar</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" name="nama_pendaftar" placeholder="Nama" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">NIK</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" name="nik_pendaftar" placeholder="NIK" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
	    <div class="col-sm-8">
	      <input type="password" class="form-control" name="password" placeholder="Password" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">File</label>
	    <div class="col-sm-8">
	      <input type="file" class="form-control" name="userfile" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Tanggal Lahir</label>
	    <div class="col-sm-8">
	      <input type="date" class="form-control" name="tlahir_pendaftar" placeholder="kelurahan" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Agama</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="agama_pendaftar" required>
	      	<option>Islam</option>
	      	<option>Kristen</option>
	      	<option>Katholik</option>
	      	<option>Budha</option>
	      	<option>Hindu</option>
	      	<option>Konghucu</option>
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Alamat</label>
	    <div class="col-sm-8">
	      <input type="text" name="alamat_pendaftar" class="form-control" required>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Jarak Desa ke Puskesmas</label>
	    <div class="col-sm-8">
	      <select name="datakriteria[jarak_1]" class="form-control" required>
	      	<?php $model_umum->getList('Jarak 1'); ?>
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Status Wilayah domisili ke Puskesmas</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[jarak_2]" required>
		    	<?php $model_umum->getList('Jarak 2');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Usia</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[evaluasi_diri_1]" required>
		    	<?php $model_umum->getList('Evaluasi Diri 1');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Pendidikan</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[evaluasi_diri_2]" required>
		    	<?php $model_umum->getList('Evaluasi Diri 2');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Prestasi Akademik</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[evaluasi_diri_3]" required>
		    	<?php $model_umum->getList('Evaluasi Diri 3');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Status Perkawinan</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[evaluasi_diri_4]" required>
		    	<?php $model_umum->getList('Evaluasi Diri 4');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Riwayat Pekerjaan</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[pengalaman_kerja_1]" required>
		    	<?php $model_umum->getList('Pengalaman Kerja 1');?>
		  </select>
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputPassword3" class="col-sm-4 control-label">Masa Kerja</label>
	    <div class="col-sm-8">
	      <select class="form-control" name="datakriteria[pengalaman_kerja_2]" required>
		    	<?php $model_umum->getList('Pengalaman Kerja 2');?>
		    </select>
	    </div>
	</div>
	<div class="form-group">
	    <center><button type="submit" class="btn btn-success btn-lg" >Daftar</button></center>
	</div>
</form>
</div>