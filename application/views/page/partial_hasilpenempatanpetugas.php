<table class="table table-bordered">
	<tr>
		<th>Nama</th>
		<th>Kode Pendaftaran</th>
		<th>Puskesmas</th>
		<th>Lihat Data</th>
		<th>Nilai Akhir</th>
		<th>Terima</th>
	</tr>
	<?php $cek=0 ?>
	<?php foreach ($daftar_bidan as $key): $cek++;?>
		<tr>
			<td><?php echo $key->nama ?></td>
			<td><?php echo $key->kode_pendaftaran ?></td>
			<td><?php echo $key->kelurahan ?></td>
			<td><a href="<?php echo base_url() ?>admin/detilpendaftarv2/<?php echo $key->id ?>">Lihat Data</a></td>
			<td><?php echo $key->nilai_akhir ?></td>
			<td>
				<?php if ($key->status_diterima == 0): ?>
					<a href="<?php echo base_url() ?>admin/actionTerima/<?php echo $key->id ?>"><button>Terima</button></a>
				<?php else: ?>
					<a href="<?php echo base_url() ?>admin/actionBatalkan/<?php echo $key->id ?>"><button>Batalkan</button></a>
				<?php endif ?>
				
			</td>
		</tr>
	<?php endforeach ?>
	<?php if ($cek == 0): ?>
		<tr>
			<td colspan="6"><center>Data Tidak Ditemukan</center></td>
		</tr>
	<?php endif ?>
</table>