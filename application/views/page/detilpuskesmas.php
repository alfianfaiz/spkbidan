<div class="container-fluid">
	<div class="fluid-container">
	<?php foreach ($data_puskesmas as $key): ?>
	<div class="col-md-6">
		<table class="table">
			<tr>
				<td>Kelurahan</td>
				<td>:</td>
				<td><?php echo $key->kelurahan ?></td>
			</tr>
			<tr>
				<td>STATUS</td>
				<td>:</td>
				<td><?php echo $key->status ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-6">
		<table class="table">
			<tr>
				<td>
					<?php if ($key->status == 1): ?>
						<a href="<?php echo base_url() ?>admin/disablepuskesmas/<?php echo $key->id ?>">Disable</a>
	 				<?php else: ?>
						<a href="<?php echo base_url() ?>admin/enablepuskesmas/<?php echo $key->id ?>">Enable</a>
	 				<?php endif ?>
				</td>
			</tr>
			<tr>
				<td><a href="<?php echo base_url() ?>admin/editpuskesmas/<?php echo $key->id ?>">Edit Puskesmas</a></td>
			</tr>
		</table>
	</div>
	<?php endforeach ?>
</div>
</div>
<div class="container-fluid">
<h2>Data Puskemas</h2>
	<?php foreach ($model_puskesmas->getDataPuskesmas($puskesmas_id) as $key): ?>
		<table class="table">
			<tr>
				<td colspan="2"><h4>Parameter Jarak</h4></td>
			</tr>
			<tr>
				<td>Jarak Desa ke Puskesmas</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Jarak 1") ?></td>
			</tr>
			<tr>
				<td>Status Wilayah domisili ke Puskesmas</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Jarak 2") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Parameter Evaluasi Diri</h4></td>
			</tr>
			<tr>
				<td>Usia</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Evaluasi Diri 1") ?></td>
			</tr>
			<tr>
				<td>Pendidikan</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Evaluasi Diri 2") ?></td>
			</tr>
			<tr>
				<td>Prestasi Akademik</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Evaluasi Diri 3") ?></td>
			</tr>
			<tr>
				<td>Status Perkawinan</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Evaluasi Diri 4") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Parameter Pengalaman Kerja</h4></td>
			</tr>
			<tr>
				<td>Riwayat Pekerjaan</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Pengalaman Kerja 1") ?></td>
			</tr>
			<tr>
				<td>Masa Kerja</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Pengalaman Kerja 2") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Uji Kompetensi</h4></td>
			</tr>
			<tr>
				<td>UK Asuhan Persalinan Normal</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Uji Kompetensi 1") ?></td>
			</tr>
			<tr>
				<td>UK Resusitasi Bayi</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Uji Kompetensi 2") ?></td>
			</tr>
			<tr>
				<td>UK Pencegahan Infeksi</td>
				<td>:</td>
				<td><?php echo $model_puskesmas->getKelasKriteriaPuskemas($key->id,"Uji Kompetensi 3") ?></td>
			</tr>
		</table>
	<?php endforeach ?>
</div>