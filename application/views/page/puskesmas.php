<div class="container-fluid">
	<h2>Data Puskesmas Kecamatan Gunungpati</h2>
	<form style="width:400px" class="pull-right">
		<a href="<?php echo base_url() ?>admin/tambah_puskesmas" class="btn">Tambah Puskesmas</a>
	  	<input type="text" name="search">
	  	<button>Cari</button>
	 </form>
	 <div class="row">
	 	<table class="table">
	 		<tr>
	 			<td>No.</td>
	 			<td>Nama Kelurahan</td>
	 			<td>Aksi</td>
	 		</tr>
	 		<?php $no = 0; ?>
	 		<?php foreach ($data_puskesmas as $key): $no++; ?>
	 			<tr>
		 			<td><?php echo $no ?></td>
		 			<td><?php echo $key->kelurahan ?></td>
		 			<td>
		 				<?php if ($key->status == 1): ?>
							<a href="<?php echo base_url() ?>admin/detilpuskesmas/<?php echo $key->id ?>">Detil</a> | <a href="<?php echo base_url() ?>admin/disablepuskesmas/<?php echo $key->id ?>">Disable</a>
		 				<?php else: ?>
							<a href="<?php echo base_url() ?>admin/detilpuskesmas/<?php echo $key->id ?>">Detil</a> | <a href="<?php echo base_url() ?>admin/enablepuskesmas/<?php echo $key->id ?>">Enable</a>
		 				<?php endif ?>
		 			</td>
		 		</tr>
	 		<?php endforeach ?>
	 	</table>
	 </div>
</div>