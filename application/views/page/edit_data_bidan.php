<?php 
	$data_bidan = $model_bidan->getDataBidan($bidan_id);
 ?>
<div class="container-fluid">
	<form class="form-horizontal" action="<?php echo base_url() ?>admin/actionEditDataBidan" method="post">
		<?php foreach ($data_bidan as $key): ?>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label" >Kelurahan</label>
			    <div class="col-sm-8">
			      <select name="puskesmas_id" class="form-control" required>
			      	<?php $model_umum->getListPuskesmas($key->puskesmas_id); ?>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Nama Pendaftar</label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control" name="nama_pendaftar" value="<?php echo $key->nama ?>" placeholder="Nama" required>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">NIK</label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control" name="nik_pendaftar" value="<?php echo $key->nik ?>" placeholder="NIK" required>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Password</label>
			    <div class="col-sm-8">
			      <input type="password" class="form-control" name="password" value="<?php echo $key->password ?>" placeholder="Password" required>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Agama</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="agama_pendaftar" required>
			      	<option>Islam</option>
			      	<option>Kristen</option>
			      	<option>Katholik</option>
			      	<option>Budha</option>
			      	<option>Hindu</option>
			      	<option>Konghucu</option>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Alamat</label>
			    <div class="col-sm-8">
			      <input type="text" name="alamat_pendaftar" class="form-control" value="<?php echo $key->alamat ?>" required>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Jarak Desa</label>
			    <div class="col-sm-8">
			      <select name="datakriteria[jarak_1]" class="form-control" required>
			      	<?php $model_umum->getList('Jarak 1',$model_bidan->getIDKelasKriteriaBidan($key->id,'Jarak 1') ); ?>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Jarak Desa 2</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[jarak_2]" required>
				    	<?php $model_umum->getList('Jarak 2',$model_bidan->getIDKelasKriteriaBidan($key->id,'Jarak 2') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Evaluasi 1</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[evaluasi_diri_1]" required>
				    	<?php $model_umum->getList('Evaluasi Diri 1',$model_bidan->getIDKelasKriteriaBidan($key->id,'Evaluasi Diri 1') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Evaluasi 2</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[evaluasi_diri_2]" required>
				    	<?php $model_umum->getList('Evaluasi Diri 2',$model_bidan->getIDKelasKriteriaBidan($key->id,'Evaluasi Diri 2') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Evaluasi 3</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[evaluasi_diri_3]" required>
				    	<?php $model_umum->getList('Evaluasi Diri 3',$model_bidan->getIDKelasKriteriaBidan($key->id,'Evaluasi Diri 3') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Evaluasi 4</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[evaluasi_diri_4]" required>
				    	<?php $model_umum->getList('Evaluasi Diri 4',$model_bidan->getIDKelasKriteriaBidan($key->id,'Evaluasi Diri 4') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Pengalaman Kerja 1</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[pengalaman_kerja_1]" required>
				    	<?php $model_umum->getList('Pengalaman Kerja 1',$model_bidan->getIDKelasKriteriaBidan($key->id,'Pengalaman Kerja 1') );?>
				  </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Pengalaman Kerja 2</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[pengalaman_kerja_2]" required>
				    	<?php $model_umum->getList('Pengalaman Kerja 2',$model_bidan->getIDKelasKriteriaBidan($key->id,'Pengalaman Kerja 2') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Uji Kompetensi 1</label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[uji_kompetensi_1]" required>
				    	<?php $model_umum->getList('Uji Kompetensi 1',$model_bidan->getIDKelasKriteriaBidan($key->id,'Uji Kompetensi 1') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Uji Kompetensi </label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[uji_kompetensi_2]" required>
				    	<?php $model_umum->getList('Uji Kompetensi 2',$model_bidan->getIDKelasKriteriaBidan($key->id,'Uji Kompetensi 2') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputPassword3" class="col-md-3 control-label">Uji Kompetensi </label>
			    <div class="col-sm-8">
			      <select class="form-control" name="datakriteria[uji_kompetensi_3]" required>
				    	<?php $model_umum->getList('Uji Kompetensi 3',$model_bidan->getIDKelasKriteriaBidan($key->id,'Uji Kompetensi 3') );?>
				    </select>
			    </div>
			</div>
			<div class="form-group">
			<input type="hidden" name="bidan_id" value="<?php echo $key->id ?>">
			<input type="hidden" name="kode_pendaftaran" value="<?php echo $key->kode_pendaftaran ?>">
			    <center><button type="submit" class="btn btn-success btn-lg" >Simpan</button></center>
			</div>
		<?php endforeach ?>
	</form>

</div>