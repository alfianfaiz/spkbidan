<div class="container-fluid">
	<form class="form-horizontal" method="post" action="<?php echo base_url() ?>admin/simpanpuskesmasbaru/">
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Kelurahan</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="kelurahan" placeholder="kelurahan">
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Jarak Desa 1</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="jarak1">
			    	<?php $model_umum->getList('Jarak 1');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Jarak Desa 2</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="jarak2">
			    	<?php $model_umum->getList('Jarak 2');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Evaluasi 1</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="evaluasi1">
			    	<?php $model_umum->getList('Evaluasi Diri 1');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Evaluasi 2</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="evaluasi2">
			    	<?php $model_umum->getList('Evaluasi Diri 2');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Evaluasi 3</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="evaluasi3">
			    	<?php $model_umum->getList('Evaluasi Diri 3');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Evaluasi 4</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="evaluasi4">
			    	<?php $model_umum->getList('Evaluasi Diri 4');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Pengalaman Kerja 1</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="pengalamankerja1">
			    	<?php $model_umum->getList('Pengalaman Kerja 1');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Pengalaman Kerja 2</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="pengalamankerja2">
			    	<?php $model_umum->getList('Pengalaman Kerja 2');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Uji Kompetensi 1</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="ujikompetensi1">
			    	<?php $model_umum->getList('Uji Kompetensi 1');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Uji Kompetensi 2</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="ujikompetensi2">
			    	<?php $model_umum->getList('Uji Kompetensi 2');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Uji Kompetensi 3</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="ujikompetensi3">
			    	<?php $model_umum->getList('Uji Kompetensi 3');?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
			<button class="btn btn-info">Simpan </button>
		</div>
	</form>
</div>