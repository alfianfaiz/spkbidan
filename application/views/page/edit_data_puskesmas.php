<?php 
	$data_puskesmas = $model_puskesmas->getDataPuskesmas($puskesmas_id);
 ?>
<div class="container-fluid">
	<form class="form-horizontal" action="<?php echo base_url() ?>admin/actionEditDataPuskesmas" method="post">
		<?php foreach ($data_puskesmas as $key): ?>
			<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Kelurahan</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="kelurahan" placeholder="kelurahan" value="<?php echo $key->kelurahan ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Jarak Desa ke Puskesmas</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[jarak_1]">
			    	<?php $model_umum->getList('Jarak 1',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Jarak 1') );?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Status Wilayah domisili ke Puskesmas</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[jarak_2]">
			    	<?php $model_umum->getList('Jarak 2',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Jarak 2'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Usia</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[evaluasi_diri_1]">
			    	<?php $model_umum->getList('Evaluasi Diri 1',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Evaluasi Diri 1'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Pendidikan</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[evaluasi_diri_2]">
			    	<?php $model_umum->getList('Evaluasi Diri 2',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Evaluasi Diri 2'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Prestasi Akademik</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[evaluasi_diri_3]">
			    	<?php $model_umum->getList('Evaluasi Diri 3',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Evaluasi Diri 3'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Status Perkawinan</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[evaluasi_diri_4]">
			    	<?php $model_umum->getList('Evaluasi Diri 4',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Evaluasi Diri 4'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Riwayat Pekerjaan</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[pengalaman_kerja_1]">
			    	<?php $model_umum->getList('Pengalaman Kerja 1',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Pengalaman Kerja 1'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">Masa Kerja</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[pengalaman_kerja_2]">
			    	<?php $model_umum->getList('Pengalaman Kerja 2',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Pengalaman Kerja 2'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">UK Asuhan Persalinan Normal</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[uji_kompetensi_1]">
			    	<?php $model_umum->getList('Uji Kompetensi 1',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Uji Kompetensi 1'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">UK Resusitasi Bayi</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[uji_kompetensi_2]">
			    	<?php $model_umum->getList('Uji Kompetensi 2',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Uji Kompetensi 2'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-2 control-label">UK Pencegahan Infeksi</label>
		    <div class="col-sm-10">
		      <select class="form-control" name="data_puskesmas[uji_kompetensi_3]">
			    	<?php $model_umum->getList('Uji Kompetensi 3',$model_puskesmas->getIDKelasKriteriaPuskesmas($key->id,'Uji Kompetensi 3'));?>
			    </select>
		    </div>
		</div>
		<div class="form-group">
			<input type="hidden" name="puskesmas_id" value="<?php echo $key->id ?>">
			<button class="btn btn-info">Simpan </button>
		</div>
		<?php endforeach ?>
	</form>

</div>