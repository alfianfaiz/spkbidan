<div class="fluid-container">
	<?php 
		$data_bidan = $model_bidan->getDataBidan($this->session->userdata('id_user'));
		$nilai_akhir = 0;
		$status_diterima = 0;
		foreach ($data_bidan as $key) {
			$nilai_akhir = $key->nilai_akhir;
			$status_diterima = $key->status_diterima;
		}
	 ?>

	 <?php if ($status_diterima != 0): ?>
	 	<div class="panel panel-success" class="margin_panel">
	      <div class="panel-heading">Status Penerimaan</div>
	      <div class="panel-body">Selamat Anda Diterima di Puskesmas dengan skor : <?php echo $nilai_akhir ?></div>
	    </div>

	 <?php else: ?>

	 	<div class="panel panel-danger" id="margin_panel">
	      <div class="panel-heading">Status Penerimaan</div>
	      <div class="panel-body">Mohon Maaf, anda belum berkesempatan menjadi bidan di puskesmas kami dengan skor : <?php echo $nilai_akhir ?></div>
	    </div>
	<?php endif ?>
</div>