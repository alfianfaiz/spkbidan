<div class="div_panduan">

<div class="jumbotron">
  <h1><span class="label label-success">1</span></h1>
  <h1>Pendaftaran</h1>
  <p>Pertama, masuklah kedalam menu pendaftaran untuk melakukan pendaftaran bidan. Isikan semua identitas serta kriteria yang diperlukan dan juga puskemas yang dipilih. Setelah itu, anda akan mendapat pemberitahuan pendaftaran berhasil</p>
</div>

<div class="jumbotron">
  <h1><span class="label label-success">2</span></h1>
  <h1>Login</h1>
  <p>Masukkan NIK serta password pada saat pendaftaran lalu lik tombol login. Setelah itu, anda akan menuju halaman user.</p>
</div>

<div class="jumbotron">
  <h1><span class="label label-success">3</span></h1>
  <h1>Data Pribadi</h1>
  <p>Klik menu data pribadi di panel sebelah kiri, maka anda akan menuju halaman data pribadi. Data meliputi nama, NIK, Kode Pendaftaran, dll.</p>
</div>

<div class="jumbotron">
  <h1><span class="label label-success">4</span></h1>
  <h1>Hasil Penempatan</h1>
  <p>Klik menu hasil penempatan untuk melihat hasil penempatan berdasarkan kecocokan kriteria. Anda akan melihat apakah diterima pada puskesmas yang diinginkan atau tidak</p>
</div>

<div class="jumbotron">
  <h1><span class="label label-success">5</span></h1>
  <h1>Logout</h1>
  <p>Klik menu Logout untuk keluar dari menu user<p>
</div>


</div>