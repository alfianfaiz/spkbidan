<?php 
	$data = array(
		'model_bidan' =>$model_bidan,
		'bidan_id' => $bidan_id,
		'action' => true,
		);
 ?>
<div class="container-fluid">
	<?php $this->load->view('page/partial_profil', $data); ?>
</div>
<div class="container-fluid">
<h2>Data Pendaftar</h2>
	<?php foreach ($model_bidan->getDataBidan($bidan_id) as $key): ?>
		<table class="table">
			<tr>
				<td colspan="2"><h4>Parameter Jarak</h4></td>
			</tr>
			<tr>
				<td>Jarak Desa ke Puskesmas</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Jarak 1") ?></td>
			</tr>
			<tr>
				<td>Status Wilayah domisili ke Puskesmas</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Jarak 2") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Parameter Evaluasi Diri</h4></td>
			</tr>
			<tr>
				<td>Usia</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Evaluasi Diri 1") ?></td>
			</tr>
			<tr>
				<td>Pendidikan</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Evaluasi Diri 2") ?></td>
			</tr>
			<tr>
				<td>Prestasi Akademik</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Evaluasi Diri 3") ?></td>
			</tr>
			<tr>
				<td>Status Perkawinan</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Evaluasi Diri 4") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Parameter Pengalaman Kerja</h4></td>
			</tr>
			<tr>
				<td>Riwayat Pekerjaan</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Pengalaman Kerja 1") ?></td>
			</tr>
			<tr>
				<td>Masa Kerja</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Pengalaman Kerja 2") ?></td>
			</tr>
			<tr>
				<td colspan="2"><h4>Uji Kompetensi</h4></td>
			</tr>
			<tr>
				<td>UK Asuhan Persalinan Normal</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Uji Kompetensi 1") ?></td>
			</tr>
			<tr>
				<td>UK Resusitasi Bayi</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Uji Kompetensi 2") ?></td>
			</tr>
			<tr>
				<td>UK Pencegahan Infeksi</td>
				<td>:</td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,"Uji Kompetensi 3") ?></td>
			</tr>
		</table>
	<?php endforeach ?>
</div>