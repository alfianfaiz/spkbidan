<?php 
	$data = array('daftar_bidan'=>$daftar_bidan);
	$data_puskesmas = $this->db->get('puskesmas')->result();
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5"><h3>Data Pendaftar Hasil Seleksi</h3></div>
		<div class="col-md-6 pull-right" style="padding-top:30px;">
			<form method="post" action="#">
				<select id="findpuskesmas">
					<?php foreach ($data_puskesmas as $key): ?>
						<option value="<?php echo $key->id ?>"><?php echo $key->kelurahan ?></option>
					<?php endforeach ?>
				</select>
				<input type="text" name="keyword">
				<button type="submit">Cari</button>
			</form>
		</div>
	</div>
</div>
<br>
<div class="container-fluid" id="tablependaftar">
	<?php 
		$this->load->view('page/partial_hasilpenempatanpetugas',$data );
	 ?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#findpuskesmas').change(function(){
			var puskesmas_id = document.getElementById('findpuskesmas').value;
			$.ajax({
				type:'POST',
				data:{puskesmas_id:puskesmas_id},
				//LOAD URL 
				url:'<?php echo base_url();?>home/daftarBidanMendaftar',
				success:function(msg){
					$('#tablependaftar').html(msg);
					// alert(msg);
				},
				error:function(msg){
					alert('gagal');
				}
			})
		})
	})
</script>

