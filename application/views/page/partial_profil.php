
<?php $asset = base_url().'assets/'; foreach ($model_bidan->getDataBidan($bidan_id) as $key): ?>

<div class="container">

	<div class="row">

	<div class="col-md-12">
	
		<img class="imageku marginku" src="<?php echo $asset ?>image/<?php echo $key->image ?>" alt="..." class="img-circle" width="200" height="200">
	
	</div>

	<div class="col-md-12">
			<table class="table table-bordered" id="tabelku">
				<tr class="danger">
					<td>Nama</td>
					
					<td><?php echo $key->nama ?></td>
				</tr>
				<tr class="success">
					<td>NIK</td>
					
					<td><?php echo $key->nik ?></td>
				</tr>
				<tr class="info">
					<td>Kode Pendaftaran</td>
					
					<td><?php echo $key->kode_pendaftaran ?></td>
				</tr>
				<tr class="danger">
					<td>Mendaftar di Puskesmas</td>
					
					<td><?php echo $key->puskesmas_id ?></td>
				</tr>
				<tr class="success">
					<td>Agama</td>
					
					<td><?php echo $key->agama ?></td>
				</tr>
				<tr class="info">
					<td>Tanggal Lahir</td>
					
					<td><?php echo $key->tanggal_lahir ?></td>
				</tr>
				<tr class="danger">
					<td>Alamat</td>
					
					<td><?php echo $key->alamat ?></td>
				</tr>
			</table>
		
	</div>
	<?php if (isset($action)): ?>
		<div class="col-md-12">
		<div class="border_aksi">
		<div class="row">
			<div class="col-md-12">
			<!-- <table class="table table-bordered" id="aksi"> -->
				<!-- <tr> -->
					<span id="aksi_judul">Aksi</span>
				<!-- </tr> -->
				</div>
				<!-- <tr> -->
				<div class="col-md-2">
					<span><a class="btn btn-danger btn-lg " href="<?php echo base_url() ?>admin/hapusbidan/<?php echo $key->id ?>">Hapus</a></span>
				<!-- </tr> -->
				</div>
				<!-- <tr>
					<td> -->
					<div class="col-md-2">
						<?php if ($key->status_diterima == 0): ?>
							<a class="btn btn-primary btn-lg margin_aksi" href="<?php echo base_url() ?>admin/actionTerima/<?php echo $key->id ?>">Terima</a>
						<?php else: ?>
							<a class="btn btn-primary btn-lg margin_aksi" href="<?php echo base_url() ?>admin/actionBatalkan/<?php echo $key->id ?>">Batalkan</a>
						<?php endif ?>
					</div>
					<!-- </td>
				</tr> -->
				<!-- <tr> -->
				<div class="col-md-2 margin_aksi" >
					<!-- <td> --><a class="btn btn-success btn-lg margin_aksi" href="<?php echo base_url() ?>admin/editDataBidan/<?php echo $key->id ?>">Edit Data</a><!-- </td> -->
				</div>
				<!-- </tr> -->
			<!-- </table> -->

			</div>
			</div>
		</div>
	<?php endif ?>
	</div>
	</div>
	<?php endforeach ?>
 