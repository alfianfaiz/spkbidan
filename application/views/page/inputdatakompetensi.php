<div class="container-fluid">
	<center><h2 class="marginku">Input Data Kompetensi</h2></center>
	<form class="form-horizontal" style="width:600px" action="<?php echo base_url() ?>admin/actioninputdatakompetensi" method="post">
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-4 control-label">Kode Pendaftaran</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" name="kode_pendaftaran" placeholder="Kode Pendaftaran" required>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-4 control-label">Kompetensi Asuhan Persalinan</label>
		    <div class="col-sm-8">
		      <select name="kap" class="form-control">
		     	<?php $model_umum->getList('Uji Kompetensi 1');?>
		     </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-4 control-label">Kompetensi Resustansi</label>
		    <div class="col-sm-8">
		     <select name="kr" class="form-control">
		     	<?php $model_umum->getList('Uji Kompetensi 2');?>
		     </select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputPassword3" class="col-sm-4 control-label">Kompetensi Pencegahan Infeksi</label>
		    <div class="col-sm-8">
			    <select name="kpi" class="form-control">
			    	<?php $model_umum->getList('Uji Kompetensi 3');?>
			    </select>
		    </div>
		</div>			
		<center><button type="submit" class="btn btn-info btn-lg" class="marginku">Simpan</button></cnter>
	</form>
	<table class="table" id="marginku3">
		<tr class="info">
			<td>No</td>
			<td>Nama Bidan</td>
			<td>Kode</td>
			<td>K. Asuhan Persalinan</td>
			<td>K.Resustansi</td>
			<td>K. Pencegahan Infeksi</td>
			<td>Nilai Akhir</td>
		</tr>
		<?php $nomor = 0; ?>
		<?php foreach ($model_bidan->getAllBidan() as $key): $nomor++ ?>
			<tr>
				<td><?php echo $nomor?></td>
				<td><?php echo $key->nama ?></td>
				<td><?php echo $key->kode_pendaftaran ?></td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,'Uji Kompetensi 1') ?></td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,'Uji Kompetensi 2') ?></td>
				<td><?php echo $model_bidan->getKelasKriteriaBidan($key->id,'Uji Kompetensi 3') ?></td>
				<td><?php echo $key->nilai_akhir ?></td>
			</tr>
		<?php endforeach ?>
	</table>
</div>